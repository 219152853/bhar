### A Pluto.jl notebook ###
# v0.19.22

using Markdown
using InteractiveUtils

# ╔═╡ 1ebc7008-ccaa-11ed-0d80-37c2acebfbba
using Markdown

# ╔═╡ 882287a7-2082-425e-a819-9cb990679678
using InteractiveUtils

# ╔═╡ 8439e656-e966-43c3-a4bb-0c8340f27f41
using Dates

# ╔═╡ 25d69b6c-6423-4547-af88-b6e134ddebf2
my_eg = ["1981-Jan", "1981-Feb", "1981-Mar", "1981-Apr"]

# ╔═╡ 8f0f76cf-bdc0-485c-ae47-1ac2c3e27a3d
typeof(my_eg)

# ╔═╡ f316dc82-1031-42e0-adc7-39e4eb2c4173
month_to_num = Dict("Jan" => 1, "Feb" => 2, "Mar" => 3, "Apr" => 4, "May" => 5, "Jun" => 6, "Jul" => 7, "Aug" => 8, "Sep" => 9, "Oct" => 10, "Nov" => 11, "Dec" => 12)

# ╔═╡ 90af40da-a76c-4fa1-9992-7615339ef155
function change_dates(old_dates::Vector{String}, converter::Dict{String, Int64})::Vector{Date}
	result = Vector{Date}()
	for cur_date in old_dates
		cur_date_part = split(cur_date, "-")
		the_date = Date(parse(Int, cur_date_part[1]), converter[cur_date_part[2]])
		push!(result, the_date)
	end
	return result
end

# ╔═╡ 065add2a-4c63-431e-9389-7a584e938a28
conv_eg = change_dates(my_eg, month_to_num)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Dates = "ade2ca70-3891-5945-98fb-dc099432e06a"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.8.5"
manifest_format = "2.0"
project_hash = "dd919c003c587c7e3d361191b0cea8d73114ea68"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"
"""

# ╔═╡ Cell order:
# ╠═1ebc7008-ccaa-11ed-0d80-37c2acebfbba
# ╠═882287a7-2082-425e-a819-9cb990679678
# ╠═8439e656-e966-43c3-a4bb-0c8340f27f41
# ╠═25d69b6c-6423-4547-af88-b6e134ddebf2
# ╠═8f0f76cf-bdc0-485c-ae47-1ac2c3e27a3d
# ╠═f316dc82-1031-42e0-adc7-39e4eb2c4173
# ╠═90af40da-a76c-4fa1-9992-7615339ef155
# ╠═065add2a-4c63-431e-9389-7a584e938a28
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
